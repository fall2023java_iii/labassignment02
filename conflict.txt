I got a brand new place, I think I've seen it twice all year
-This is the second half of the song.
I can't remember how it looks inside, so you can picture how my life's been
I went from staring at the same four walls for twenty one years
-Fans were initially torn on it upon release.
-Over time, however, it grew to become a favorite among many.
-It was released as a single a few months prior to the release of the album of the same name.
To seeing the whole world in just twelve months
Been gone for so long I might've just found God
