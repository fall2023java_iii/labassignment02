//Brian Kirkov
//2134488
package vehicles;

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String m, int ng, double ms) {
        this.manufacturer = m;
        this.numberGears = ng;
        this.maxSpeed = ms;
    }

    public String toString() {
        String s = "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + 
                   ", Max Speed: " + this.maxSpeed;
        return s;
    }
}