//Brian Kirkov
//2134488
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] b = new Bicycle[4];
        b[0] = new Bicycle("Mountain Bikes", 6, 25.0);
        b[1] = new Bicycle("Red Bicycles", 5, 23.0);
        b[2] = new Bicycle("Bicycle Master", 8, 27.0);
        b[3] = new Bicycle("Mike's Bikes", 7, 24.0);

        for(int i = 0; i < b.length; i++) {
            System.out.println(b[i]);
        }
    }
}
